package com.google.android.samples.dynamicfeatures

import android.content.Intent
import com.google.android.play.core.splitcompat.SplitCompatApplication
import com.google.android.samples.dynamicfeatures.di.DIAppManager
import com.google.android.samples.dynamicfeatures.di.component.empty.DaggerEmptyFlightViewComponent
import com.google.android.samples.dynamicfeatures.di.component.empty.DaggerEmptyHotelViewComponent
import com.google.android.samples.dynamicfeatures.di.module.EmptyFlightViewModule
import com.google.android.samples.dynamicfeatures.di.module.EmptyHotelViewModule

class DynamicFeatureApplication : SplitCompatApplication() {

    object DI {
        lateinit var manager: DIAppManager
    }

    override fun onCreate() {
        super.onCreate()
        DI.manager = DIAppManager()
        DI.manager.flightViewComponent = DaggerEmptyFlightViewComponent.builder()
                .emptyFlightViewModule(EmptyFlightViewModule())
                .build()

        DI.manager.hotelViewComponent = DaggerEmptyHotelViewComponent.builder()
                .emptyHotelViewModule(EmptyHotelViewModule())
                .build()

        initComponentWithService()
    }

    private fun initComponentWithService(){
        val hotelServiceClassName = "com.example.hotel.service.HotelService"
        val flightServiceClassName = "com.example.flight.service.FlightService"
        launchService(flightServiceClassName)
        launchService(hotelServiceClassName)
    }

    private fun launchService(className: String) {
        Intent().setClassName(packageName, className)
                .also {
                    startService(it)
                }
    }
}