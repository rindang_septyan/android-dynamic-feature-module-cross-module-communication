package com.google.android.samples.dynamicfeatures.di.factory

import android.content.Context
import android.view.View

interface HotelViewFactoryContract {

    fun getView(context: Context) : View
}