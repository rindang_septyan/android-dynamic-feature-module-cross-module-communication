package com.google.android.samples.dynamicfeatures.di.component.base

import com.google.android.samples.dynamicfeatures.di.factory.HotelViewFactoryContract

interface HotelViewComponent {
    fun hotelViewFactory() : HotelViewFactoryContract
}