package com.google.android.samples.dynamicfeatures.di.component.empty

import com.google.android.samples.dynamicfeatures.di.component.base.HotelViewComponent
import com.google.android.samples.dynamicfeatures.di.module.EmptyHotelViewModule
import dagger.Component

@Component(modules = [EmptyHotelViewModule::class])
interface EmptyHotelViewComponent : HotelViewComponent