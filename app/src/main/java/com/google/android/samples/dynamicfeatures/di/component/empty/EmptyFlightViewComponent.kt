package com.google.android.samples.dynamicfeatures.di.component.empty

import com.google.android.samples.dynamicfeatures.di.component.base.FlightViewComponent
import com.google.android.samples.dynamicfeatures.di.module.EmptyFlightViewModule
import dagger.Component

@Component(modules = [EmptyFlightViewModule::class])
interface EmptyFlightViewComponent : FlightViewComponent