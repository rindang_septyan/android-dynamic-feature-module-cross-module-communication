package com.google.android.samples.dynamicfeatures.di

import com.google.android.samples.dynamicfeatures.di.component.base.FlightViewComponent
import com.google.android.samples.dynamicfeatures.di.component.base.HotelViewComponent

class DIAppManager {
    lateinit var hotelViewComponent: HotelViewComponent
    lateinit var flightViewComponent: FlightViewComponent
}