package com.google.android.samples.dynamicfeatures.di.module

import android.content.Context
import android.view.View
import android.widget.TextView
import com.google.android.samples.dynamicfeatures.di.factory.HotelViewFactoryContract
import dagger.Module
import dagger.Provides

@Module
class EmptyHotelViewModule {

    @Provides
    fun provideHotelViewFactory() = object :  HotelViewFactoryContract{
        override fun getView(context: Context): View = TextView (context).apply { text = "Empty Hotel View" }
    }
}