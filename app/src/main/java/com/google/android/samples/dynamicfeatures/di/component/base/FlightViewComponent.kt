package com.google.android.samples.dynamicfeatures.di.component.base

import com.google.android.samples.dynamicfeatures.di.factory.FlightViewFactoryContract
import dagger.Component

interface FlightViewComponent {
    fun flightViewFactory(): FlightViewFactoryContract
}