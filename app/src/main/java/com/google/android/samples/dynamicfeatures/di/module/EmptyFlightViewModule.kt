package com.google.android.samples.dynamicfeatures.di.module

import android.content.Context
import android.view.View
import android.widget.TextView
import com.google.android.samples.dynamicfeatures.di.factory.FlightViewFactoryContract
import dagger.Module
import dagger.Provides

@Module
class EmptyFlightViewModule {

    @Provides
    fun provideFlightViewFactory() = object : FlightViewFactoryContract {
        override fun getView(context: Context): View = TextView(context).apply { text = "Empty Flight View" }
    }
}