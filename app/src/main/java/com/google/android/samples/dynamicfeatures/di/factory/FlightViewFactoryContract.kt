package com.google.android.samples.dynamicfeatures.di.factory

import android.content.Context
import android.view.View

interface FlightViewFactoryContract {

    fun getView(context: Context) : View

}