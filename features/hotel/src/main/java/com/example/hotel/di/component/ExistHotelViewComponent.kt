package com.example.flight.di.component

import com.example.flight.di.module.HotelViewModule
import com.google.android.samples.dynamicfeatures.di.component.base.HotelViewComponent
import dagger.Component

@Component(modules = [HotelViewModule::class])
interface ExistHotelViewComponent : HotelViewComponent