package com.example.flight.di.module

import com.example.flight.di.factory.HotelViewFactory
import com.google.android.samples.dynamicfeatures.di.factory.FlightViewFactoryContract
import com.google.android.samples.dynamicfeatures.di.factory.HotelViewFactoryContract
import dagger.Module
import dagger.Provides

@Module
class HotelViewModule {

    @Provides
    fun provideHotelViewFactory() : HotelViewFactoryContract = HotelViewFactory()
}
