package com.example.hotel.service

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.example.flight.di.component.DaggerExistHotelViewComponent
import com.example.flight.di.module.HotelViewModule
import com.google.android.samples.dynamicfeatures.DynamicFeatureApplication

class HotelService : Service() {

    private val binder = MyBinder()

    override fun onCreate() {
        super.onCreate()
        initComponent()
        Log.d("MyService","Hotel Service")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        stopSelf()
        return Service.START_NOT_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    private fun initComponent() {
        DynamicFeatureApplication.DI.manager.hotelViewComponent = DaggerExistHotelViewComponent.builder()
                .hotelViewModule(HotelViewModule())
                .build()
    }

    inner class MyBinder : Binder() {
        internal val service: HotelService
            get() = this@HotelService
    }
}