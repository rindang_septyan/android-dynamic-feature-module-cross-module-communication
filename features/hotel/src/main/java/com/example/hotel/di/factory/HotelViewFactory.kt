package com.example.flight.di.factory

import android.content.Context
import android.view.View
import com.example.hotel.R
import com.google.android.samples.dynamicfeatures.di.factory.HotelViewFactoryContract

class HotelViewFactory : HotelViewFactoryContract {
    override fun getView(context: Context): View = View.inflate(context, R.layout.hotel, null)
}