package com.example.topfeature

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import android.widget.FrameLayout
import com.example.flight.di.factory.FlightViewFactory

class TopFeatureActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val layout = FrameLayout(this).apply {
            layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            addView(FlightViewFactory().getView(this@TopFeatureActivity))
            setBackgroundColor(Color.parseColor("#aabbcc"))
        }
        setContentView(layout)
    }
}