package com.example.flight.di.factory

import android.content.Context
import android.view.View
import com.example.flight.R
import com.google.android.samples.dynamicfeatures.di.factory.FlightViewFactoryContract

class FlightViewFactory : FlightViewFactoryContract {
    override fun getView(context: Context): View = View.inflate(context, R.layout.flight, null)
}