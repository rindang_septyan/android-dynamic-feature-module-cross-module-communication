package com.example.flight.di.component

import com.example.flight.di.module.FlightViewModule
import com.google.android.samples.dynamicfeatures.di.component.base.FlightViewComponent
import com.google.android.samples.dynamicfeatures.di.module.EmptyFlightViewModule
import dagger.Component

@Component(modules = [FlightViewModule::class])
interface ExistFlightViewComponent : FlightViewComponent