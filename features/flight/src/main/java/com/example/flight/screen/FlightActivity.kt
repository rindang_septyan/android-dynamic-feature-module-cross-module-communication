package com.example.flight.screen

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.LinearLayout
import com.example.flight.di.component.DaggerExistFlightViewComponent
import com.example.flight.di.module.FlightViewModule
import com.google.android.samples.dynamicfeatures.DynamicFeatureApplication


class FlightActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val layourParam = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        val linearLayout = LinearLayout(this).apply {
            layoutParams = layourParam
            orientation = LinearLayout.VERTICAL
            addView(DynamicFeatureApplication.DI.manager.flightViewComponent.flightViewFactory().getView(this@FlightActivity))
            addView(DynamicFeatureApplication.DI.manager.hotelViewComponent.hotelViewFactory().getView(this@FlightActivity))
        }
        setContentView(linearLayout)
    }

}