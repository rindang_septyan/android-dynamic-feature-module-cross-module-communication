package com.example.flight.di.module

import com.example.flight.di.factory.FlightViewFactory
import com.google.android.samples.dynamicfeatures.di.factory.FlightViewFactoryContract
import dagger.Module
import dagger.Provides

@Module
class FlightViewModule {

    @Provides
    fun provideFlightViewFactory() : FlightViewFactoryContract = FlightViewFactory()
}
