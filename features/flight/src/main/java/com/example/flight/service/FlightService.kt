package com.example.flight.service

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.example.flight.di.component.DaggerExistFlightViewComponent
import com.example.flight.di.module.FlightViewModule
import com.google.android.samples.dynamicfeatures.DynamicFeatureApplication

class FlightService : Service() {

    private val binder = MyBinder()

    override fun onCreate() {
        super.onCreate()
        initComponent()
        Log.d("MyService","Flight Service")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        stopSelf()
        return Service.START_NOT_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    private fun initComponent() {
        DynamicFeatureApplication.DI.manager.flightViewComponent = DaggerExistFlightViewComponent.builder()
                .flightViewModule(FlightViewModule())
                .build()
    }

    inner class MyBinder : Binder() {
        internal val service: FlightService
            get() = this@FlightService
    }
}